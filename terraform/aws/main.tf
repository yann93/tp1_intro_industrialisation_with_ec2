resource "aws_key_pair" "deployer" {
    key_name = "ma_clefs"
    public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDguK5LRaOVY7sYdsyC1/1G+USpOrG8er149ty1jYwMhU66AE2D0j42Kw29y1uBOe5ysK99Ob+zcGh4eOMgkspQbuxbitOovZYCHaalhU6XvOUgIaxe4agYIWzIH2HsTBglxlTERoMOTfKae9bBGr7DC0rmISejVO7wB3sDRRtYhBkTHMiytExdVvfOYjgQ0N0MJIa+/vYH7vnF0/S7FV9jkXme05qowykAM0Kz7KbJYZV0ZlTPO+MiSUtq3c4ukg5+QLuczSFOg3cX+YSDO53Et77oRpLt5WOpWV2Q9xD1r2oCCmvAMkC0Gym5quGdDOVUP3bukrYo/Yunw38wtTZrctpVIG3u7W1L1NtdF02qpc3bca2YW1WIJvQqZNH7FqaoK4rbbR7/7Gv6RTdwv0jf5aeLoveqECSYRNFEHTLUddTel6beNdMVK1hQzQUEBjVu+BpLjtNojeuRm3/9iEimkTcUbxK3kw01+sxw+BoojvNrXzJ5dMqJywY2KHONqlM= yannbron@LAPTOP-BDFM11QO"
    }

resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}

resource "aws_security_group" "security_group" {
    name = "security_group_test"

    egress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     = ["0.0.0.0/0"]
    }

    ingress {
        from_port   = 22
        to_port     = 80
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_instance" "my_ec2_instance" {
    ami = data.aws_ami.ubuntu.id
    instance_type = "t2.micro"
    vpc_security_group_ids = [aws_security_group.security_group.id]
    key_name = "ma_clefs"
    
    tags = {
        Name = "Cloud instance"
    }
}

resource "aws_dynamodb_table" "basic-dynamodb-table" {
  name           = "GameScores"
  billing_mode   = "PROVISIONED"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "UserId"
  range_key      = "GameTitle"

  attribute {
    name = "UserId"
    type = "S"
  }

  attribute {
    name = "GameTitle"
    type = "S"
  }

  attribute {
    name = "TopScore"
    type = "N"
  }

  global_secondary_index {
    name               = "GameTitleIndex"
    hash_key           = "GameTitle"
    range_key          = "TopScore"
    write_capacity     = 10
    read_capacity      = 10
    projection_type    = "INCLUDE"
    non_key_attributes = ["UserId"]
  }

  tags = {
    Name        = "dynamodb-table-1"
    Environment = "production"
  }
}

resource "aws_iam_role" "role" {
  name = "test-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}


resource "aws_iam_policy" "policy" {
  name        = "test-policy"
  description = "A test policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
        "Sid": "ListAndDescribe",
        "Effect": "Allow",
        "Action": [
            "dynamodb:List*",
            "dynamodb:DescribeReservedCapacity*",
            "dynamodb:DescribeLimits",
            "dynamodb:DescribeTimeToLive"
        ],
        "Resource": "*"
    },
    {
        "Sid": "SpecificTable",
        "Effect": "Allow",
        "Action": [
            "dynamodb:BatchGet*",
            "dynamodb:DescribeStream",
            "dynamodb:DescribeTable",
            "dynamodb:Get*",
            "dynamodb:Query",
            "dynamodb:Scan",
            "dynamodb:BatchWrite*",
            "dynamodb:CreateTable",
            "dynamodb:Delete*",
            "dynamodb:Update*",
            "dynamodb:PutItem"
        ],
        "Resource": "arn:aws:dynamodb:*:*:table/GameScores"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "test-attach" {
  role       = aws_iam_role.role.name
  policy_arn = aws_iam_policy.policy.arn
}